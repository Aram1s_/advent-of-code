pub fn run(s: &str) -> i64 {
    part_2(s)
}

fn part_1(s: &str) -> i64 {
    s.lines().next().unwrap()["seeds: ".len()..]
        .split(" ")
        .map(|m| m.parse().unwrap())
        .collect::<Vec<i64>>()
        .iter()
        .map(|i| {
            let mut input = i.clone();
            for map_type in &parser(s) {
                input = map_input(map_type, input);
            }
            input
        })
        .min()
        .unwrap()
}

fn part_2(s: &str) -> i64 {
    let mut seeds = s.lines().next().unwrap()["seeds: ".len()..]
        .split(" ")
        .map(|m| m.parse().unwrap())
        .collect::<Vec<i64>>();
    let mut pairs = Vec::new();
    while seeds.len() > 1 {
        pairs.push((seeds.pop().unwrap(), seeds.pop().unwrap()));
    }
    let mut lowest: i64 = 100000000;
    pairs.iter().for_each(|p| {
        println!("range: {}", p.0);
        (p.1..p.1 + p.0).for_each(|i| {
            if i % 10000 == 0 {
                println!("i: {}", i);
            }
            let mut input = i.clone();
            for map_type in &parser(s) {
                input = map_input(map_type, input);
            }
            if input < lowest {
                lowest = input;
            }
        })
    });
    lowest
}

fn map_input(maps: &Vec<Map>, input: i64) -> i64 {
    for map in maps {
        if input >= map.source && input < map.source + map.range {
            return map.dest + (input - map.source);
        }
    }
    input
}

fn parser(s: &str) -> Vec<Vec<Map>> {
    s[7..]
        .split("\n\n")
        .map(|m| {
            m.lines()
                .skip(1)
                .map(|m| {
                    let iter: Vec<i64> = m.split(" ").map(|m| m.parse::<i64>().unwrap()).collect();
                    let mut iter = iter.iter();
                    Map {
                        dest: *iter.next().unwrap(),
                        source: *iter.next().unwrap(),
                        range: *iter.next().unwrap(),
                    }
                })
                .collect()
        })
        .collect()
}
#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = "seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4";

    #[test]
    fn example_1() {
        assert_eq!(part_1(EXAMPLE), 35);
    }

    #[test]
    fn example_2() {
        assert_eq!(part_2(EXAMPLE), 46);
    }
}

struct Map {
    dest: i64,
    source: i64,
    range: i64,
}
