pub fn run(s: &str) -> u32 {
    let s = s
        .replace("one", "o1ne")
        .replace("two", "tw2o")
        .replace("three", "thre3e")
        .replace("four", "f4our")
        .replace("five", "5five")
        .replace("six", "6six")
        .replace("seven", "7seven")
        .replace("eight", "eigh8t")
        .replace("nine", "9nine");
    println!("s: {}", s);
    s.split('\n')
        .map(|m| m.chars().filter(|c| c.is_ascii_digit()))
        .filter(|m| m.clone().next().is_some())
        .map(|m| m.clone().next().unwrap().to_string() + &m.last().unwrap().to_string())
        .filter_map(|d| d.parse::<u32>().ok())
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn day_1() {
        assert_eq!(
            run(
                "1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet
one
a
"
            ),
            153
        );
        assert_eq!(run("seven1cvdvnhpgthfhfljmnq"), 71);
        assert_eq!(
            run(
                "two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen"
            ),
            281
        );
    }
}
