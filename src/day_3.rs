pub fn run(s: &str) -> u32 {
    part_two(s)
}

fn part_one(s: &str) -> u32 {
    let schematic: Vec<Vec<char>> = s.lines().map(|m| m.chars().collect()).collect();
    let mut result = 0;
    for i in 0..schematic.len() {
        let mut j = 0;
        while j < schematic[i].len() {
            if schematic[i][j].is_ascii_digit() {
                let mut length = 1;
                let mut number = schematic[i][j].to_string();
                while (j + length) < schematic[i].len() && schematic[i][j + length].is_ascii_digit()
                {
                    number += &schematic[i][j + length].to_string();
                    length += 1;
                }
                let number = number.parse::<u32>().unwrap();
                for l in 0..length {
                    if l == 0 {
                        if j as i32 - 1 >= 0 && i as i32 - 1 >= 0 {
                            if schematic[i - 1][j - 1] != '.' {
                                result += number;
                                break;
                            }
                        }
                        if j as i32 - 1 >= 0 && i as i32 + 1 < schematic.len() as i32 - 1 {
                            if schematic[i + 1][j - 1] != '.' {
                                result += number;
                                break;
                            }
                        }
                        if j as i32 - 1 >= 0 {
                            if schematic[i][j - 1] != '.' {
                                result += number;
                                break;
                            }
                        }
                    }
                    if l == length - 1 {
                        if j + l + 1 < schematic[i].len() && i as i32 - 1 >= 0 {
                            if schematic[i - 1][j + l + 1] != '.' {
                                result += number;
                                break;
                            }
                        }
                        if j + l + 1 < schematic[i].len()
                            && i as i32 + 1 < schematic.len() as i32 - 1
                        {
                            if schematic[i + 1][j + l + 1] != '.' {
                                result += number;
                                break;
                            }
                        }
                        if j + l + 1 < schematic[i].len() {
                            if schematic[i][j + l + 1].is_ascii_digit() {
                                panic!("something went wrong with the length");
                            }
                            if schematic[i][j + l + 1] != '.' {
                                result += number;
                                break;
                            }
                        }
                    }
                    if i + 1 < schematic.len() - 1 {
                        if schematic[i + 1][j + l] != '.' {
                            result += number;
                            break;
                        }
                    }
                    if i as i32 - 1 >= 0 {
                        if schematic[i - 1][j + l] != '.' {
                            result += number;
                            break;
                        }
                    }
                }
                j += length;
            } else {
                j += 1;
            }
        }
    }
    result
}

fn part_two(s: &str) -> u32 {
    let schematic: Vec<Vec<char>> = s.lines().map(|m| m.chars().collect()).collect();
    let mut result = 0;
    for i in 0..schematic.len() {
        for j in 0..schematic[i].len() {
            if schematic[i][j] == '*' {
                let mut numbers = Vec::new();
                if j as i32 - 1 >= 0 && i as i32 - 1 >= 0 {
                    if schematic[i - 1][j - 1].is_ascii_digit() {
                        numbers.push(get_number(&schematic, i as i32 - 1, j as i32 - 1));
                    }
                }
                if j as i32 - 1 >= 0 && i as i32 + 1 < schematic.len() as i32 - 1 {
                    if schematic[i + 1][j - 1].is_ascii_digit() {
                        numbers.push(get_number(&schematic, i as i32 + 1, j as i32 - 1));
                    }
                }
                if j + 1 < schematic[i].len() && i as i32 - 1 >= 0 {
                    if schematic[i - 1][j + 1].is_ascii_digit() {
                        numbers.push(get_number(&schematic, i as i32 - 1, j as i32 + 1));
                    }
                }
                if j + 1 < schematic[i].len() && i as i32 + 1 < schematic.len() as i32 - 1 {
                    if schematic[i + 1][j + 1].is_ascii_digit() {
                        numbers.push(get_number(&schematic, i as i32 + 1, j as i32 + 1));
                    }
                }
                if j as i32 - 1 >= 0 {
                    if schematic[i][j - 1].is_ascii_digit() {
                        numbers.push(get_number(&schematic, i as i32, j as i32 - 1));
                    }
                }
                if j + 1 < schematic[i].len() {
                    if schematic[i][j + 1].is_ascii_digit() {
                        numbers.push(get_number(&schematic, i as i32, j as i32 + 1));
                    }
                }
                if i + 1 < schematic.len() {
                    if schematic[i + 1][j].is_ascii_digit() {
                        numbers.push(get_number(&schematic, i as i32 + 1, j as i32));
                    }
                }
                if i as i32 - 1 >= 0 {
                    if schematic[i - 1][j].is_ascii_digit() {
                        numbers.push(get_number(&schematic, i as i32 - 1, j as i32));
                    }
                }
                for i in 0..numbers.len() {
                    for j in 0..numbers.len() {
                        if j < numbers.len() && i < numbers.len() && i != j {
                            if numbers[i] == numbers[j] {
                                numbers.remove(i);
                            }
                        }
                    }
                }
                for (n, _, _) in &numbers {
                    print!("n: {}, ", n);
                }
                if numbers.len() == 2 {
                    let (a, _, _) = numbers[0];
                    let (b, _, _) = numbers[1];
                    result += a * b;
                    print!("ratio = {}", a * b);
                }
                println!("");
            }
        }
    }
    result
}

fn get_number(schematic: &Vec<Vec<char>>, i: i32, j: i32) -> (u32, i32, i32) {
    let mut offset = -1;
    while j + offset >= 0 && schematic[i as usize][(j + offset) as usize].is_ascii_digit() {
        offset -= 1;
    }
    offset += 1;
    let mut number = schematic[i as usize][(j + offset) as usize].to_string();
    let mut length = 1;
    while j + length < schematic[i as usize].len() as i32
        && schematic[i as usize][(j + length + offset) as usize].is_ascii_digit()
    {
        number += &schematic[i as usize][(j + length + offset) as usize].to_string();
        length += 1;
    }
    (number.parse().unwrap(), i, j + offset)
}

#[cfg(test)]
mod tests {
    use super::*;
    const EXAMPLE: &str = "467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..";
    #[test]
    fn example_1() {
        assert_eq!(part_one(EXAMPLE), 4361);
    }
    #[test]
    fn example_2() {
        assert_eq!(part_two(EXAMPLE), 467835);
    }
}
