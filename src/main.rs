mod day_5;
fn main() {
    let s = std::fs::read_to_string("input/day_5.txt").unwrap();
    println!("{}", day_5::run(&s));
}
