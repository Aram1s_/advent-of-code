pub fn run(s: &str) -> u32 {
    let a: Vec<Vec<Vec<&str>>> = s
        .trim_end()
        .split('\n')
        .map(|m| noe(m))
        .map(|m| m.split(';').map(|m| m.split(',').collect()).collect())
        .collect();
    let mut result = 0;
    for (i, line) in a.iter().enumerate() {
        let mut game_cubes = Vec::new();
        for round in line {
            let mut round_cubes = Vec::new();
            for b in round {
                round_cubes.push(parser(b));
            }
            game_cubes.push(round_cubes);
        }
        result += sum_cubes(&game_cubes).1;
    }
    result as u32
}
fn noe(s: &str) -> &str {
    for (i, c) in s.chars().enumerate() {
        if c == ':' {
            return &s[i + 1..];
        }
    }
    s
}

fn parser(s: &str) -> Cube {
    let a = s[1..].split(" ").collect::<Vec<&str>>();
    let b = a[1];
    let c = a[0];
    match b {
        "red" => Cube::Red(c.parse().unwrap()),
        "green" => Cube::Green(c.parse().unwrap()),
        "blue" => Cube::Blue(c.parse().unwrap()),
        _ => {
            println!("{}", b);
            println!("{}", c);
            panic!("f parse");
        }
    }
}

fn sum_cubes(game: &Vec<Vec<Cube>>) -> (bool, u32) {
    let mut largest = SumCube {
        red: 0,
        green: 0,
        blue: 0,
    };
    for round in game {
        let mut sum = SumCube {
            red: 0,
            green: 0,
            blue: 0,
        };
        for cube in round {
            match cube {
                Cube::Red(v) => {
                    sum.red += v;
                    if largest.red < *v {
                        largest.red = *v;
                    }
                }
                Cube::Green(v) => {
                    sum.green += v;
                    if largest.green < *v {
                        largest.green = *v;
                    }
                }
                Cube::Blue(v) => {
                    sum.blue += v;
                    if largest.blue < *v {
                        largest.blue = *v;
                    }
                }
            }
        }
    }
    let impossible = largest.red > 12 || largest.green > 13 || largest.blue > 14;
    (!impossible, largest.red * largest.green * largest.blue)
}

#[derive(Debug)]
struct SumCube {
    red: u32,
    green: u32,
    blue: u32,
}

#[derive(Debug)]
enum Cube {
    Red(u32),
    Green(u32),
    Blue(u32),
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn day_2() {}
}
