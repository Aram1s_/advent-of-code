pub fn run(s: &str) -> u32 {
    part_one(s)
}

fn part_one(s: &str) -> u32 {
    let mut points = 0;
    let mut card_n = 1;
    s.lines()
        .map(|m| {
            m.split("|")
                .map(|m| {
                    m.split(" ")
                        .filter(|m| m.parse::<u32>().is_ok())
                        .map(|m| m.parse::<u32>().unwrap())
                        .collect()
                })
                .collect()
        })
        .collect::<Vec<Vec<Vec<u32>>>>()
        .iter()
        .for_each(|card| {
            let (mut card_points, winning, numbers) = (0, &card[0], &card[1]);
            numbers.iter().for_each(|n| {
                winning.contains(&n).then(|| card_points += 1);
            });
            (card_points != 0)
                .then(|| println!("card points: {}, nr: {}", (2 as u32).pow(card_points - 1), card_n));
            (card_points == 0)
                .then(|| println!("card points: 0, nr: {}", card_n));
            (card_points != 0).then(|| points += (2 as u32).pow(card_points - 1));
            card_n += 1;
        });
    points
}

#[cfg(test)]
mod tests {
    use super::*;
    const EXAMPLE: &str = "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11";
    const TEST: &str = "Card 1: 41 48 83 86 17 | 83 86  6 41 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11";
    #[test]
    fn example_1() {
        assert_eq!(part_one(EXAMPLE), 13);
        assert_eq!(part_one(TEST), 21);
    }
    // #[test]
    // fn parsing() {
    //     assert!(" ".parse::<u32>().is_err());
    //     assert!("|".parse::<u32>().is_err());
    //     assert!("1:".parse::<u32>().is_err());
    //     assert!("Card".parse::<u32>().is_err());
    // }

    // #[test]
    // fn example_2() {
    // }
}
